'use strict'

$('.js').mouseover(function () {
	var index = $('.js').index(this);
	switch (index) {
		case index = 0:
			$(this).css('background-color', 'red')
			break;
		case index = 1:
			$(this).css('transform', 'scale(1.2)')
			break;
		case index = 2:
			$(this).addClass('js' + index);
			$(this).css('transform', 'scale(1.2)')
			break;
	}
});
$('.js').mouseleave(function () {
	var index = $('.js').index(this);
	switch (index) {
		case index = 0:
			$(this).css('backgroundColor', 'darkorange')
			break;
		case index = 1:
			$(this).css('transform', 'scale(1)')
			break;
		case index = 2:
			$(this).css('transform', 'scale(1)')
			break;
	}
});

function clock() {
	let now = new Date();
	let h = now.getHours();
	let min = now.getMinutes();
	let sec = now.getSeconds();
	if (min < 10) min = "0" + min;
	if (sec < 10) sec = "0" + sec;
	$('#clock_time').html(h + ":" + min + ":" + sec);
	$('#clock_frame').css('fontSize', window.innerWidth / 10 + "px");
}
setInterval(clock, 1000);

window.onload = function () {
	setInterval(slider, 2000);
}
const slide_page = $('.baner li');
let slider_num = 1;

function slider() {
	switch (slider_num) {
		case 1:
			$(slide_page[0]).css({
				top: '0',
				zIndex: '4'
			});
			$(slide_page[1]).css({
				top: '150px',
				zIndex: '3'
			});
			$(slide_page[2]).css({
				top: '150px',
				zIndex: '1'
			});
			$(slide_page[3]).css({
				top: '-150px',
				zIndex: '2'
			});
			slider_num++;
			break;
		case 2:
			$(slide_page[0]).css({
				top: '-150px',
				zIndex: '2'
			});
			$(slide_page[1]).css({
				top: '0',
				zIndex: '4'
			});
			$(slide_page[2]).css({
				top: '150px',
				zIndex: '3'
			});
			$(slide_page[3]).css({
				top: '150px',
				zIndex: '1'
			});
			slider_num = 3;
			break;
		case 3:
			$(slide_page[0]).css({
				top: '150px',
				zIndex: '1'
			});
			$(slide_page[1]).css({
				top: '-150px',
				zIndex: '2'
			});
			$(slide_page[2]).css({
				top: '0',
				zIndex: '4'
			});
			$(slide_page[3]).css({
				top: '150px',
				zIndex: '3'
			});
			slider_num = 4;
			break;
		case 4:
			$(slide_page[0]).css({
				top: '150px',
				zIndex: '1'
			});
			$(slide_page[1]).css({
				top: '150px',
				zIndex: '2'
			});
			$(slide_page[2]).css({
				top: '-150px',
				zIndex: '3'
			});
			$(slide_page[3]).css({
				top: '0px',
				zIndex: '4'
			});
			slider_num = 1;
			break;
	}
}
//04
$('.close').on('click', function () {
	$('.modal').css('display', 'none');
});
$('.modal_open').on('click', function () {
	$('.modal').css('display', 'flex');
});

//06
const tab_dis = document.getElementsByClassName('tab_main');
$('.tab_list').on('click', function () {
	let tab_index = $('.tab_list').index(this);
	$('.tab_main').css('display', 'none');
	$('.tab_main').eq(tab_index).css('display', 'block');
});
//
