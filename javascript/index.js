'use strict'

//01
const material = document.getElementsByClassName('js');
for (let i = 0; i < material.length; i++) {
	material[i].addEventListener('mouseover', () => {
		switch (i) {
			case i = 0:
				material[i].style.backgroundColor = "red"
				break;
			case i = 1:
				material[i].style.transform = "scale(1.2)"
				break;
			case i = 2:
				material[i].classList.add('js' + i);
				material[i].style.transform = "scale(1.2)"
				break;
		}
	}, false);
	material[i].addEventListener('mouseleave', () => {
		switch (i) {
			case i = 0:
				material[i].style.backgroundColor = "darkorange"
				break;
			case i = 1:
				material[i].style.transform = "scale(1)"
				break;
			case i = 2:
				material[i].style.transform = "scale(1)"
				break;
		}
	}, false);
}
//02
function clock() {
	let now = new Date();
	let h = now.getHours();
	let min = now.getMinutes();
	let sec = now.getSeconds();
	if (min < 10) min = "0" + min;
	if (sec < 10) sec = "0" + sec;
	document.getElementById("clock_time").innerHTML = h + ":" + min + ":" + sec;
	document.getElementById("clock_frame").style.fontSize = window.innerWidth / 10 + "px";
}
setInterval(clock, 1000);

window.onload = () => {
	//1000ミリ秒（1秒）毎に関数「showNowDate()」を呼び出す
	setInterval("slider()", 2000);
}

//03
var slider_body = document.getElementsByClassName('baner');
var slide_page = slider_body[0].getElementsByTagName('li');
var slider_num = 1;
//現在時刻を表示する関数
function slider() {
	switch (slider_num) {
		case 1:
			slide_page[0].style.top = '0',
				slide_page[1].style.top = '150px',
				slide_page[2].style.top = '150px',
				slide_page[3].style.top = '-150px',
				slide_page[0].style.zIndex = '4',
				slide_page[1].style.zIndex = '3',
				slide_page[2].style.zIndex = '1',
				slide_page[3].style.zIndex = '2',
				slider_num++;
			break;
		case 2:
			slide_page[0].style.top = '-150px',
				slide_page[1].style.top = '0',
				slide_page[2].style.top = '150px',
				slide_page[3].style.top = '150px',
				slide_page[0].style.zIndex = '3',
				slide_page[1].style.zIndex = '4',
				slide_page[2].style.zIndex = '1',
				slide_page[3].style.zIndex = '2',
				slider_num = 3;
			break;
		case 3:
			slide_page[0].style.top = '150px',
				slide_page[1].style.top = '-150px',
				slide_page[2].style.top = '0',
				slide_page[3].style.top = '150px',
				slide_page[0].style.zIndex = '1',
				slide_page[1].style.zIndex = '2',
				slide_page[2].style.zIndex = '4',
				slide_page[3].style.zIndex = '3',
				slider_num = 4;
			break;
		case 4:
			slide_page[0].style.top = '150px',
				slide_page[1].style.top = '150px',
				slide_page[2].style.top = '-150px',
				slide_page[3].style.top = '0',
				slide_page[0].style.zIndex = '1',
				slide_page[1].style.zIndex = '2',
				slide_page[2].style.zIndex = '3',
				slide_page[3].style.zIndex = '4',
				slider_num = 1;
			break;
	}
}

//04
const modal = document.getElementsByClassName('modal');
const close = document.getElementsByClassName('close');
const open = document.getElementsByClassName('modal_open');
close[0].addEventListener('click', () => {
	modal[0].style.display = 'none';
}, false);
open[0].addEventListener('click', () => {
	modal[0].style.display = 'flex';
}, false);

//06
var tab = document.getElementsByClassName('tab_list');
var tab_dis = document.getElementsByClassName('tab_main');
for (let i = 0; i < tab.length; i++) {
	tab[i].addEventListener('click', () => {
		for (let j = 0; j < tab_dis.length; j++) {
			tab_dis[j].style.display = 'none';
		}
		tab_dis[i].style.display = 'block';
	}, false);
}
